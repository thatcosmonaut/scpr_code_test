require 'json'
require 'rest-client'
require 'sinatra'
require 'coffee-script'
require 'barista'
require 'open-uri'

class App < Sinatra::Base
  register Barista::Integration::Sinatra
  Barista::Framework.register 'app', 'public/javascripts' if defined?(Barista::Framework)

  def filename_from_url(url)
    File.basename(URI.parse(url).path, '.mp3')
  end

  def trim_audio(full_filepath, trimmed_filepath)
    system "ffmpeg -t 10 -i #{full_filepath} -acodec copy #{trimmed_filepath}"
  end

  def download_file(url, full_filepath)
    open(full_filepath, 'wb') do |file|
      file << open(url).read
    end
  end

  def retrieve_trimmed_file(url)
    filename = filename_from_url(url)
    full_filepath = "./tmp/#{filename}-full.mp3"
    trimmed_filepath = "./tmp/#{filename}-trimmed.mp3"

    unless File.exist?(trimmed_filepath)
      download_file url, full_filepath
      trim_audio full_filepath, trimmed_filepath
      system "rm #{full_filepath}"
    end

    trimmed_filepath
  end

  get '/' do
    haml :index
  end

  post '/download' do
    content_type :json
    ng_params = JSON.parse(request.body.read)
    trimmed_filepath = retrieve_trimmed_file(ng_params['url'])
    {'download_link' => trimmed_filepath}.to_json
  end

  get '/tmp/:filename' do |filename|
    send_file "./tmp/#{filename}", filename: filename, type: 'application/octet-stream'
  end
end
